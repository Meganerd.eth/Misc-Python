from PIL import Image

# Source: https://stackoverflow.com/questions/9166400/convert-rgba-png-to-rgb-with-pil

png = Image.open('original.png').convert('RGBA')
background = Image.new('RGBA', png.size, (255,255,255,0))

alpha_composite = Image.alpha_composite(background, png)

#alpha_composite.save('foo.jpg', 'JPEG', quality=100)
alpha_composite.save('foo.png', 'PNG', quality=100)