#!C:\Python27\python.exe
# Filename: Rexel.py
# -*- coding: utf-8 -*-

import os
import sys
import re
import time
import xlwt
import xlrd
from xlrd import open_workbook
from Templates import *
import platform

OS = platform.system()
Config_Directory = 'Configs'


''' Modifiable Settings '''
Alliant_Sheet_A = 'Alliant Rexel Order Request Template.xlsx'
Alliant_Sheet_B = 'Operation inventory tracking - 11-28-16.xlsx'
''''''''''''''''''''''''''

def Valid_IP(IP=None):
    try:
        Octet = IP.split('.')
        Valid = [int(b) for b in Octet]
        Valid = [b for b in Valid if b >= 0 and b<=255]
        return len(Octet) == 4 and len(Valid) == 4
    except:
        return False

def Sanitize_IP(IP=None):
    try:
        Octet = IP.split('.')
        Valid = [int(b) for b in Octet]
        Valid = [b for b in Valid if b >= 0 and b<=255]
        return len(Octet) == 4 and len(Valid) == 4
    except:
        return False
    
def Create_Directory(Directory, Debug=False):
    ''' Input directory to create, returns boolean '''
    try:
        if OS.upper() == 'WINDOWS':
            if len(str(Directory)) < 250:
                if not os.path.exists(Directory):
                    os.makedirs(Directory)
                    if Debug == True:
                        print '[+] Directory ', str(Directory), 'created.'
                    return True
            else:
                if Debug == True:
                    print '[!] Directory name too long for Windows file-system'
                return False
        elif OS.upper() == 'LINUX':
            if not os.path.exists(Directory):
                os.makedirs(Directory)
                if Debug == True:
                    print '[+] Directory ', str(Directory), 'created.'
                return True
    except:
        if Debug == True:
            print '[!] Got exception: While trying to create', str(Directory)
        return False

def Get_Column_By_Letter(Column='A'):
    ''' Get excel column by letter '''
    
    Column = Column.upper()
    
    # Column element references
    Alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'

    if len(Column) == 1:
        # Just one letter
        for n in range(0, len(Alphabet)):
            if Alphabet[n] == Column:
                return n

    elif len(Column) == 2:
        # 2 letters eg AF
        for n in range(0, len(Alphabet)):
            if Alphabet[n] == Column[0]:
                First = n
        
        for n in range(0, len(Alphabet)):
            if Alphabet[n] == Column[1]:
                Second = n+26
        
        return First+Second

# Creat log directory
Create_Directory(Config_Directory)

# Open XLS file
Workbook = open_workbook(Alliant_Sheet_A)

# Get first sheet in Workbook
Sheet = Workbook.sheet_by_index(0)

Alliant_Sheet_A_Data = [] # Placeholder; relevant sheet data

# Verify header matches
Rexel_ID_Header = Sheet.cell(2, 0)

if not 'REXEL' in str(Rexel_ID_Header).upper():
    # Does not match
    raw_input('[!] Unable to find Rexel Site ID header - wrong file?\n')
    sys.exit()

elif 'REXEL' in str(Rexel_ID_Header).upper():
    # Header matches
    # Read all site IDs, keep reference to which vertical cell they are
    
    # Read vertically until column is empty
    nRow = 3 # start cell
    for vert_column in range(0, 9001):
        try:
            Rexel_ID = Sheet.cell(nRow, 0).value.encode('ascii','ignore')
            
        except(IndexError):
            break # End of row cells
        
        except(AttributeError):
            Rexel_ID = Sheet.cell(nRow, 0).value
            
            #import pdb;pdb.set_trace()
        
        try:
            Switch_Equipment = Sheet.cell(nRow, 10).value.encode('ascii','ignore')
            Router_Equipment = Sheet.cell(nRow, 11).value.encode('ascii','ignore')
            Equipment_Notes = Sheet.cell(nRow, 11).value.encode('ascii','ignore')
            
            '''
            Switch 1 Template 24-port (Primary) 
            Switch 2 Template 24-port (Secondary)
            No Switch deployment
            
            Switch 1 Template 48-port (Primary) 
            Switch 2 Template 48-port (Secondary)
            '''
            Switch_Equipment_Data = []
            
            # Look for Switch 1; Append to tuple
            if 'Switch 1 Template 24-port' in Switch_Equipment:
                Switch_Equipment_Data.append('Switch 1 Template 24-port')

            # Look for Switch 1; Append to tuple
            if 'Switch 1 Template 48-port' in Switch_Equipment:
                Switch_Equipment_Data.append('Switch 1 Template 48-port')
            
            # Look for Switch 2; Append to tuple
            if 'Switch 2 Template 24-port' in Switch_Equipment:
                Switch_Equipment_Data.append('Switch 2 Template 24-port')

            # Look for Switch 2; Append to tuple
            if 'Switch 2 Template 48-port' in Switch_Equipment:
                Switch_Equipment_Data.append('Switch 2 Template 48-port')
            
            # Look for Switch 3; Append to tuple
            if 'Switch 3 Template 24-port' in Switch_Equipment:
                Switch_Equipment_Data.append('Switch 3 Template 24-port')

            # Look for Switch 3; Append to tuple
            if 'Switch 3 Template 48-port' in Switch_Equipment:
                Switch_Equipment_Data.append('Switch 3 Template 48-port')
            
            # Look for Switch 4; Append to tuple
            if 'Switch 4 Template 24-port' in Switch_Equipment:
                Switch_Equipment_Data.append('Switch 4 Template 24-port')

            # Look for Switch 4; Append to tuple
            if 'Switch 4 Template 48-port' in Switch_Equipment:
                Switch_Equipment_Data.append('Switch 4 Template 48-port')
 
            '''
            Rexel DMVPN Spoke Config without Meraki
            Rexel DMVPN Spoke Config with Meraki
            No Router deployment
            '''
            Router_Equipment_Data = ''
            
            if 'without Meraki'.upper() in Router_Equipment.upper():
                # Both switches for order
                Router_Equipment_Data = 'Without Meraki'
                
            elif 'with Meraki'.upper() in Router_Equipment.upper():
                # Secondary switch only
                Router_Equipment_Data = 'With Meraki'
                
            elif 'No Router'.upper() in Router_Equipment.upper():
                # Secondary switch only
                Router_Equipment_Data = 'No Router'
                
            elif Router_Equipment.upper() == '':
                Router_Equipment_Data = ['BLANK']
                
            else:
                print '[!] Cannot identify router while parsing XLS file'
                Router_Equipment_Data = Router_Equipment
                
            # Alliant_Sheet_A_Data
            # [nRow, str(Rexel_ID).upper(), Switch_Equipment_Data, Router_Equipment_Data, Equipment_Notes]
            # [23, '1058', ['Switch 2 Template 24-port'], 'Without Meraki', 'Rexel DMVPN Spoke Config without Meraki']
            Alliant_Sheet_A_Data.append([nRow, str(Rexel_ID).upper(), Switch_Equipment_Data, Router_Equipment_Data, Equipment_Notes])
            
            nRow += 1

        except:
            print '[!] Exception while parsing XLS file'
        
#########################################
''' Rexel file 2 - Operation inventory tracking '''

# Regexp
Config_Var_Regexp = '<Column ([^>0123456789]+)>' #'<Column (.+)>'

# Open XLS file
Workbook = open_workbook(Alliant_Sheet_B)

# Get third sheet in Workbook
Sheet = Workbook.sheet_by_index(2)

Alliant_Sheet_B_Data = [] # Placeholder; relevant sheet data

# Verify header matches
Rexel_ID_Header = Sheet.cell(0, 3)
# text:u'Rexel Site ID'

##################################

Order_Info = '' # Placeholder

# User input Rexel ID(s)
User_Rexel_ID = raw_input('\n[+] Input Rexel ID\n[+] ')

# Get input Rexel ID Info from Alliant_Sheet_A_Data tuple

for nrow_Rexel in Alliant_Sheet_A_Data:
    #print nrow_Rexel
    
    if str(nrow_Rexel[1]).upper() == User_Rexel_ID.upper():
        # if element Rexel ID == User input Rexel ID
        Order_Info = nrow_Rexel

if not Order_Info == '':
    print '\n[+] Order info for Rexel ID %s' % User_Rexel_ID
    print '[+] [nRow, Rexel_ID, Switch_Equipment_Data, Router_Equipment_Data, Equipment_Notes]'
    print '[+]', Order_Info, '\n'
    #raw_input('')

else:
    print '[!] Cannot find Rexel ID %s\n' % User_Rexel_ID
    raw_input('')
    sys.exit()

# Search File B for Rexel ID & info from nRow

if not 'REXEL' in str(Rexel_ID_Header).upper():
    # Does not match
    raw_input('[!] Unable to find Rexel Site ID header - wrong file?\n')
    sys.exit()

elif 'REXEL' in str(Rexel_ID_Header).upper():
    # Header matches
    # Read all site IDs, keep reference to which vertical cell they are
    
    # Read vertically until column is empty
    nRow = 1 # start cell
    for vert_column in range(0, 9001):
        try:
            Rexel_ID = Sheet.cell(nRow, 3).value.encode('ascii','ignore')
            
        except(IndexError):
            break # End of row cells
        
        except(AttributeError):
            Rexel_ID = Sheet.cell(nRow, 3).value
            
        try:
            #print Rexel_ID
            if Rexel_ID == User_Rexel_ID:
                Matched_nRow = nRow
                raw_input('')

        except:
            print '[!] Got exception while parsing information from Sheet B'
            raw_input('\n')
            sys.exit()
            
        nRow += 1

'''
Switch 1 Template 24-port (Primary) 
Switch 2 Template 24-port (Secondary)
No Switch deployment

Switch 1 Template 48-port (Primary) 
Switch 2 Template 48-port (Secondary)
'''

def Get_Template_Information(Template=None, Template_Name=''):
    ''' Get template information from Templates '''
    if Template == None:
        return None
    
    if type(Template) == str:
        Template_Columns = re.findall(Config_Var_Regexp, Template)#[0].replace(' ', '')
        print '[*]', [Template_Name, Template_Columns]
        return [Template_Name, Template_Columns]
        
        
    elif type(Template) == list:
        # Multiple templates
        pass
    
'''
Templates = [Switch_1_24_Port,
             Switch_1_48_Port,
             Switch_2_24_Port,
             Switch_2_48_Port,
             Switch_3_24_Port,
             Switch_3_48_Port,
             Switch_4_24_Port,
             Switch_4_48_Port,
             With_Meraki,
             Without_Meraki]
'''

Switch_Templates = [] # Placeholder
Switch_Template = [] # Placeholder

# Check switch templates to use
if len(Order_Info[2]) > 1:
    # More than 1 switch template
    for element in Order_Info[2]:
        # Iterate switch templates to use

        if element == 'Switch 1 Template 24-port':
            Template_Template = Get_Template_Information(Template=Templates[0], Template_Name='Switch 1 Template 24-port')
        elif element == 'Switch 1 Template 48-port':
            Template_Template = Get_Template_Information(Template=Templates[1], Template_Name='Switch 1 Template 48-port')
        elif element == 'Switch 2 Template 24-port':
            Template_Template = Get_Template_Information(Template=Templates[2], Template_Name='Switch 2 Template 24-port')
        elif element == 'Switch 2 Template 48-port':
            Template_Template = Get_Template_Information(Template=Templates[3], Template_Name='Switch 2 Template 48-port')
        elif element == 'Switch 3 Template 24-port':
            Template_Template = Get_Template_Information(Template=Templates[4], Template_Name='Switch 3 Template 24-port')
        elif element == 'Switch 3 Template 48-port':
            Template_Template = Get_Template_Information(Template=Templates[5], Template_Name='Switch 3 Template 48-port')
        elif element == 'Switch 4 Template 24-port':
            Template_Template = Get_Template_Information(Template=Templates[6], Template_Name='Switch 4 Template 24-port')
        elif element == 'Switch 4 Template 48-port':
            Template_Template = Get_Template_Information(Template=Templates[7], Template_Name='Switch 4 Template 48-port')
        elif element == 'No Switch deployment':
            pass
        
        # Template_Template = ['Switch 1 Template 24-port', ['AB', 'AA', 'AT', 'N', 'AU', 'AV']]
        for letter_column in Template_Template[1]:
            letter_column_number = Get_Column_By_Letter(Column=letter_column)
            print '[+] ', Sheet.cell(Matched_nRow, letter_column_number).value
        
            Switch_Template.append(Sheet.cell(Matched_nRow, letter_column_number).value.encode('ascii','ignore'))
        
        print '\n'
        # Switch_Templates
        # [
        # [['Switch 1 Template 24-port', ['AB', 'AA', 'AT', 'N', 'AU', 'AV']], 
        # ['REX-Edwardsville-IL-1071-SW01', '10.70.185.102', '255.255.254.0', '10.70.185.99', '10.70.185.0', '0.0.1.255']
        # ], 
        # [['Switch 2 Template 24-port', ['AE', 'AD', 'AT', 'N', 'AU', 'AV']], ['REX-Edwardsville-IL-1071-SW02', '10.70.185.103', '255.255.254.0', '10.70.185.99', '10.70.185.0', '0.0.1.255']]
        # ]
        Switch_Templates.append([Template_Template, Switch_Template])
        Switch_Template = []

elif len(Order_Info[2]) == 1:
    # Just 1 switch template
    if Order_Info[2] == 'Switch 1 Template 24-port':
        Template_Template = Get_Template_Information(Template=Templates[0], Template_Name='Switch 1 Template 24-port')
    elif Order_Info[2] == 'Switch 1 Template 48-port':
        Template_Template = Get_Template_Information(Template=Templates[1], Template_Name='Switch 1 Template 48-port')
    elif Order_Info[2] == 'Switch 2 Template 24-port':
        Template_Template = Get_Template_Information(Template=Templates[2], Template_Name='Switch 2 Template 24-port')
    elif Order_Info[2] == 'Switch 2 Template 48-port':
        Template_Template = Get_Template_Information(Template=Templates[3], Template_Name='Switch 2 Template 48-port')
    elif Order_Info[2] == 'Switch 3 Template 24-port':
        Template_Template = Get_Template_Information(Template=Templates[4], Template_Name='Switch 3 Template 24-port')
    elif Order_Info[2] == 'Switch 3 Template 48-port':
        Template_Template = Get_Template_Information(Template=Templates[5], Template_Name='Switch 3 Template 48-port')
    elif Order_Info[2] == 'Switch 4 Template 24-port':
        Template_Template = Get_Template_Information(Template=Templates[6], Template_Name='Switch 4 Template 24-port')
    elif Order_Info[2] == 'Switch 4 Template 48-port':
        Template_Template = Get_Template_Information(Template=Templates[7], Template_Name='Switch 4 Template 48-port')
    elif Order_Info[2] == 'No Switch deployment':
        pass
    
    # Template_Template = ['Switch 1 Template 24-port', ['AB', 'AA', 'AT', 'N', 'AU', 'AV']]
    for letter_column in Template_Template[1]:
        letter_column_number = Get_Column_By_Letter(Column=letter_column)
        print Sheet.cell(Matched_nRow, letter_column_number).value
        Switch_Template.append(Sheet.cell(Matched_nRow, letter_column_number).value.encode('ascii','ignore'))

    print '\n'
    Switch_Templates = Switch_Template
        
elif len(Order_Info[2]) == 0:
    # No switch template
    print '[+] No switch templates to use for this Rexel ID - Please make sure this is correct!'
    raw_input('\n')


# Write to file

def Write_New_Config_File(Template=None, Filename=None, Data=None):
    if Template == None or Data == None or Filename == None:
        return None
    
    # Sanitize Filename ***
    
    if type(Template) == str:
        # ['AB', 'AA', 'AT', 'N', 'AU', 'AV']
        for n_col_letter in range(0, len(Data[0][1])):
            #nTemplate = re.sub('< Column '+str(Data[0][1][n_col_letter])+'>', Data[1][n_col_letter], Template)
            print '[+] Replacing %s with %s' % (str(Data[0][1][n_col_letter]), Data[1][n_col_letter])
            Template = re.sub('<Column '+str(Data[0][1][n_col_letter])+'>', Data[1][n_col_letter], Template)

        #print Template
        #raw_input('')
        
        File = open(Config_Directory+'\\'+Filename, 'w+')
        File.write(Template)
        File.close()
        

for switch_template in Switch_Templates:
    print '\n'
    print '[*]', switch_template

    if switch_template[0][0] == 'Switch 1 Template 24-port':
        Write_New_Config_File(Template=Templates[0], Filename=switch_template[1][0]+'.txt', Data=switch_template)
    elif switch_template[0][0] == 'Switch 1 Template 48-port':
        Write_New_Config_File(Template=Templates[1], Filename=switch_template[1][0]+'.txt', Data=switch_template)
    elif switch_template[0][0] == 'Switch 2 Template 24-port':
        Write_New_Config_File(Template=Templates[2], Filename=switch_template[1][0]+'.txt', Data=switch_template)
    elif switch_template[0][0] == 'Switch 2 Template 48-port':
        Write_New_Config_File(Template=Templates[3], Filename=switch_template[1][0]+'.txt', Data=switch_template)
    elif switch_template[0][0] == 'Switch 3 Template 24-port':
        Write_New_Config_File(Template=Templates[4], Filename=switch_template[1][0]+'.txt', Data=switch_template)
    elif switch_template[0][0] == 'Switch 3 Template 48-port':
        Write_New_Config_File(Template=Templates[5], Filename=switch_template[1][0]+'.txt', Data=switch_template)
    elif switch_template[0][0] == 'Switch 4 Template 24-port':
        Write_New_Config_File(Template=Templates[6], Filename=switch_template[1][0]+'.txt', Data=switch_template)
    elif switch_template[0][0] == 'Switch 4 Template 48-port':
        Write_New_Config_File(Template=Templates[7], Filename=switch_template[1][0]+'.txt', Data=switch_template)
    elif switch_template[0][0] == 'No Switch deployment':
        pass
    

# Lets do the router templates now
print '\n'

'''
Rexel DMVPN Spoke Config without Meraki
Rexel DMVPN Spoke Config with Meraki
No Router deployment
'''

Router_Template = [] # Placeholder

# Check router templates to use

if Order_Info[3] == 'Without Meraki':
    # Router without Meraki

    Router_Template = Get_Template_Information(Template=Templates[9], Template_Name='Rexel DMVPN Spoke Config without Meraki')


elif Order_Info[3] == 'With Meraki':
    # Router with Meraki

    Router_Template = Get_Template_Information(Template=Templates[8], Template_Name='Rexel DMVPN Spoke Config with Meraki')


elif Order_Info[3] == 'No Router':
    # No router
    pass

else:
    print '\n[!] Some sort of error finding / creating router template - please check manually'
    raw_input('')


Router_Template_Values = [] # Placeholder

# Router_Template = ['Rexel DMVPN Spoke Config without Meraki', ['F', 'Y', 'G', 'I', 'M', 'AT', 'N', 'Q', 'AS', 'R', 'U', 'V', 'Y', 'Y', 'K', 'O', 'S', 'Z', 'AU', 'AV']]
for letter_column in Router_Template[1]:
    letter_column_number = Get_Column_By_Letter(Column=letter_column)
    print '[+]', Sheet.cell(Matched_nRow, letter_column_number).value
    Router_Template_Values.append(Sheet.cell(Matched_nRow, letter_column_number).value.encode('ascii','ignore'))

Router_Template = [Router_Template, Router_Template_Values]
# ['Rexel DMVPN Spoke Config without Meraki', ['F', 'Y', 'G', 'I', 'M', 'AT', 'N', 'Q', 'AS', 'R', 'U', 'V', 'Y', 'Y', 'K', 'O', 'S', 'Z', 'AU', 'AV'], ['REX-Longmont-CO-3252-RTRD01', '10.71.2.151', '172.40.0.150', '172.50.0.150', '10.80.27.100', '255.255.254.0', '10.80.27.98', '10.82.149.3', '255.255.255.0', '10.82.149.1', '10.72.27.11', '10.72.27.9', '10.71.2.151', '10.71.2.151', '10.80.27.0/23', '10.82.149.0/24', '10.72.27.8/29', '10.71.2.151/32', '10.80.27.0', '0.0.1.255']]

if Router_Template[0][0] == 'Rexel DMVPN Spoke Config without Meraki':
    Write_New_Config_File(Template=Templates[9], Filename=Router_Template[1][0]+'.txt', Data=Router_Template)

elif Router_Template[0][0] == 'Rexel DMVPN Spoke Config with Meraki':
    Write_New_Config_File(Template=Templates[8], Filename=Router_Template[1][0]+'.txt', Data=Router_Template)


print '\n[+] Finished! \n\n'

import pdb;pdb.set_trace()


