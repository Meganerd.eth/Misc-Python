#!/usr/bin/python
# Filename: XLS_To_CSV.py
# -*- coding: utf-8 -*-

import xlrd
import csv
from os import sys
import glob
import re

Path = r'C:\Files\\'

def XLSX_To_CSV(excel_file):
    workbook = xlrd.open_workbook(excel_file)
    all_worksheets = workbook.sheet_names()
    for worksheet_name in all_worksheets:
        worksheet = workbook.sheet_by_name(worksheet_name)
        your_csv_file = open(''.join([worksheet_name,'.csv']), 'wb')
        wr = csv.writer(your_csv_file, quoting=csv.QUOTE_ALL)

        for rownum in xrange(worksheet.nrows):
            wr.writerow([unicode(entry).encode("utf-8") for entry in worksheet.row_values(rownum)])
        your_csv_file.close()

if __name__ == "__main__":
    #XLSX_To_CSV(sys.argv[1])

    Files = glob.glob(Path+'*.xlsx')

    for file in Files:
        print str(file)
        XLSX_To_CSV(Path+file)
        x = raw_input('\n[+] fin')
