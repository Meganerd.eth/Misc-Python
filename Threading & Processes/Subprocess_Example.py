#!/usr/bin/python
# Filename: Subprocess_Example.py
# -*- coding: utf-8 -*-

import subprocess

Example_Process = subprocess.Popen(['ping', '127.0.0.1'],\
                  stdout=subprocess.PIPE, stderr=subprocess.PIPE)

Process_Output, Process_Errors = Example_Process.communicate()