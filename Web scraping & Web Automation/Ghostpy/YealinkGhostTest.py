from ghost import Ghost, Session
import time
import re
import datetime as Date
from Common import Common

def Data_Capture_Yealink_Ghostpy(IP='', Timeout=5, Debug=False):
    ''' Data capture Yealink T-series and W5xP devices using Ghost.py (Webkit) '''

    # HTTP only with Ghost.py
    Protocol = 'http'

    Data_Capture = ['SERIAL','MAC','MODEL','PART_NUMBER',
                    'YEALINK','DEVICE_TYPE','FIRMWARE', 'PROV_URL_STRING',
                    '','SOURCEIP','TIMESTAMP','DATE',
                    '', []
                    ]

    Current_Datetime_Object = Date.datetime.now()
    Data_Capture[11] = str(Current_Datetime_Object).split(' ')[0] # Date
    Data_Capture[10] = str(Current_Datetime_Object).split(' ')[1] # Timestamp
    Data_Capture[9] = IP

    Alive = Common.Ping(IP)
    #print Alive
    if Alive[0] == False:
        print '\n[!] %s did not reply to ping' % IP
        return False

    ghost = Ghost()

    USERAGENT = "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:45.0) Gecko/20100101 Firefox/45.0"

    with ghost.start():
        session = Session(ghost, download_images=False, display=True, user_agent=USERAGENT)

        page, rs = session.open(Protocol+'://'+IP, timeout=Timeout)

        if Debug == True:
            print '[+] Trying to log in using:', 'admin', 'admin'

        session.evaluate("""
        document.querySelector('input[name="username"]').value = 'admin';
        document.querySelector('input[name="pwd"]').value = 'admin';
        """)

        session.evaluate("""document.querySelector('input[id="idConfirm"]').click();""",
                     expect_loading=True)

        #print dir(page.content)
        #print page.content.title()

        # Check for identifier in HTML
        # T-series: data == HTML
        Type = 'T'
        try:
            if 'Yealink' in page.content.data().upper():
                pass
            elif 'T23G' in page.content.data().upper():
                pass
            elif 'W52P' in page.content.data().upper():
                pass
            elif 'W56P' in page.content.data().upper():
                pass
            else:
                # Cannot identify device type
                # Likely not a yealink device
                return False
        except:
            # W52P/W56P does not use data?
            # Title == HTML
            Type = 'DECT'
            try:
                if 'Yealink' in page.content.title().upper():
                    pass
                elif 'T23G' in page.content.title().upper():
                    pass
                elif 'W52P' in page.content.title().upper():
                    pass
                elif 'W56P' in page.content.title().upper():
                    pass
                else:
                    # Cannot identify device type
                    # Likely not a yealink device
                    return False
            except:
                return False

        if Debug == True:
            print '[+] Log in successful!'

        #ghost.wait_page_loaded()
        session.wait_for_page_loaded()
        #time.sleep(2)

        page2, rs2 = session.open(Protocol+'://'+IP+'/servlet?p=status&q=load', timeout=Timeout)
        Page_HTML = page2.content

        try:
            MAC = re.findall('tdMACAddress[^>]+>([^<]+)<', Page_HTML)[0].data()
        except:
            print '[!] Cannot find MAC on %s' % IP
            MAC = 'NULL'

        Data_Capture[0] = MAC
        Data_Capture[1] = MAC
        if Debug == True:
            print '[MAC]', MAC

        try:
            Firmware = re.findall('tdFirmware[^>]+>([^<]+)<', Page_HTML)[0].data()
        except:
            print '[!] Cannot find Firmware on %s' % IP
            Firmware = 'NULL'

        Data_Capture[6] = Firmware
        if Debug == True:
            print '[Firmware]', Firmware

        try:
            URL_Title = re.findall('title\>([^<]+)<', Page_HTML)[0].data()
        except:
            print '[!] Cannot find Model on %s' % IP
            URL_Title = 'NULL'

        if 'T23G' in URL_Title:
            Model = 'T23G'
            Data_Capture[5] = 'Phone' # Device type
        elif 'W52P' in URL_Title:
            Model = 'W52P'
            Data_Capture[5] = 'Router' # Device type
        elif 'W56P' in URL_Title:
            Model = 'W56P'
            Data_Capture[5] = 'Router' # Device type
        elif 'DECT' in URL_Title:
            Model = 'W56P-W52P'
            Data_Capture[5] = 'Router' # Device type
        else:
            # Just use page title since we cant identify model
            Model = URL_Title.replace(',', '').replace('"', '').replace("'", '') # Basic sanitation

        Data_Capture[2] = Model
        Data_Capture[3] = Model
        if Debug == True:
            print '[Model]', Model

        session.wait_for_page_loaded()
        #time.sleep(2)

        # IP DECT
        # /servlet?p=phone-autoprovision&q=load

        # T-Series
        # /servlet?p=settings-autop&q=load

        if Type == 'T':
            page3, rs3 = session.open(Protocol+'://'+IP+'/servlet?p=settings-autop&q=load', timeout=Timeout)
        elif Type == 'DECT':
            page3, rs3 = session.open(Protocol+'://'+IP+'/servlet?p=phone-autoprovision&q=load', timeout=Timeout)
        Page3_HTML = page3.content

        try:
            Server_URL = re.findall('AutoProvisionServerURL.+value=\"([^"]+)', Page3_HTML)[0].data()
        except:
            # Check blank URL
            try:
                Server_URL = re.findall('AutoProvisionServerURL.+value=(\"\")', Page3_HTML)[0].data()
                if Server_URL == '""':
                    Server_URL = ''
                else:
                    print '[!] Cannot find Server URL on %s' % IP
                    Server_URL = 'NULL'
            except:
                print '[!] Cannot find Server URL on %s' % IP
                Server_URL = 'NULL'

        Data_Capture[7] = Server_URL
        if Debug == True:
            print '[Server_URL]', Server_URL

        return Data_Capture

if __name__ == '__main__':
    pass
