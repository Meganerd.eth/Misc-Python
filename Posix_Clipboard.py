#!/usr/bin/python
# Filename: Posix_Clipboard.py
# -*- coding: utf-8 -*-

''' Decode Base64 clipboard data
    Bind script to hotkeys for usage '''

import subprocess
import base64

def getClipboardData():
    p = subprocess.Popen(['xclip','-selection', 'clipboard', '-o'], stdout=subprocess.PIPE)
    retcode = p.wait()
    data = p.stdout.read()
    return data

def setClipboardData(data):
    p = subprocess.Popen(['xclip','-selection','clipboard'], stdin=subprocess.PIPE)
    p.stdin.write(data)
    p.stdin.close()
    retcode = p.wait()

# Get clipboard data
Clipboard = getClipboardData()

# TODO: Verify base64 in clipboard
Base64_In_Clipboard = True

if Base64_In_Clipboard == True:
    print '\n\n[Base64 String]\n%s' % Clipboard
    # Decode base64
    try:
        Decoded_Clipboard = base64.b64decode(Clipboard)
        # TODO: Check for malformed I/O
        # Set clipoboard with decoded data
        setClipboardData(data=Decoded_Clipboard)
        print '\n\n\n[Decoded Base64]\n%s' % Decoded_Clipboard
    except:
        print '\n[!] Invalid base64 clipboard string'