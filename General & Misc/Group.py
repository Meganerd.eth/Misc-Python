#!/usr/bin/python
# Filename: Group.py
# -*- coding: utf-8 -*-

def group(lst, n):
  for i in range(0, len(lst), n):
    val = lst[i:i+n]
    if len(val) == n:
      yield tuple(val)

'''
>>> list(group([0,3,4,10,2,3], 2))
[(0, 3), (4, 10), (2, 3)]
>>> list(group(range(10), 3))
[(0, 1, 2), (3, 4, 5), (6, 7, 8)]
'''