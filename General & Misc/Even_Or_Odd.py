#!/usr/bin/python
# Filename: Even_Or_Odd.py
# -*- coding: utf-8 -*-

''' Deterine if an integer value is even or odd '''

list = []

# Create list
for i in range(0,101):
    list.append(i)

# Iterating through the elements in list
for i in range(len(list)):
    if i % 2 == 0:
        print '[+] Even:', i
    else:
        print '[+] Odd :', i
