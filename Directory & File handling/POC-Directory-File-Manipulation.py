#!/usr/bin/python
# Filename: POC-Directory-File-Manipulation.py
# -*- coding: utf-8 -*-

import os

def Check_File_Exists(filename, Debug=False):
    ''' Input path+filename, returns boolean value if exists '''
    if os.path.isfile(filename):
        if Debug == True:
            print '[!] File already exists!'
        return True
    else:
        if Debug == True:
            print '[+] File does not exist'
        return False

def Check_Directory_Exists(Directory, Debug=False):
    ''' Input directory path, returns boolean value if exists '''
    if os.path.isdir(Directory):
        return True
    else:
        if Debug == True:
            print '[+] Directory does not exist'
        return False

def Create_Directory(Directory, Debug=False):
    ''' Create directory from given path
    return True on success
    return None is exists
    return False on exception '''
    if not os.path.exists(Directory):
        os.makedirs(Directory)
        #if Debug == True:
        print '\n[+] Directory ', Directory, 'created!'
        return True
    elif os.path.exists(Directory):
        if Debug == True:
            print '\n[+] Directory ', Directory, 'already exists!'
        return None
    else:
        if Debug == True:
            print '\n[!] Error creating directory:', Directory
        return False
