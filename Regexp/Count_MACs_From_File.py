#!/usr/bin/python
# Filename: Count_MACs_From_File.py
# -*- coding: utf-8 -*-

import re
import glob

def Count_CSV_MACs(File=None):
    MAC_List = []
    
    f = open(File, 'r')
    data = f.read()
    f.close()
    
    print '\n[+] File: %s' % str(File)

    Prefix_A_Matches = re.findall('(0059DC[0-9a-fA-F]{6})') #SPA
    Prefix_B_Matches = re.findall('(64167F[0-9a-fA-F]{6})') 
    Prefix_C_Matches = re.findall('(0004F2[0-9a-fA-F]{6})')

    All_Matches = Prefix_A_Matches + Prefix_B_Matches + Prefix_C_Matches
    
    # Remove duplicates
    All_Matches = list(set(All_Matches))
    
    print '[MACS]\n', str(len(e))
    
    for i in e:
        MAC_List.append(e)
    
    return MAC_List

if __name__ == "__main__":
    Files = glob.glob(Path+'*.csv')
    Count_CSV_MACs(Path+'988703.csv')
