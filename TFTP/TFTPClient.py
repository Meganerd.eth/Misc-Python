#!/usr/bin/python
# Filename: TFTPClient.py
# -*- coding: utf-8 -*-

if __name__ == '__main__':
    import tftpy

    client = tftpy.TftpClient('127.0.0.1', 69)
    client.download('remote_filename', 'local_filename')