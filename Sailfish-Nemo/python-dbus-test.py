#!/usr/bin/python3

import dbus

bus = dbus.SessionBus()
object = bus.get_object('org.freedesktop.Notifications','/org/freedesktop/Notifications')
interface = dbus.Interface(object,'org.freedesktop.Notifications')
#print(interface.GetCapabilities())

interface.Notify("app_name",
                 0,
                 "icon-m-notifications",
                 "Here is the title",
                 "and here the body",
                 dbus.Array(["default", ""]),
                 dbus.Dictionary({"x-nemo-preview-body": "preview body",
                                  "x-nemo-preview-summary": "preview summary"},
                                  signature='sv'),
                 0)
