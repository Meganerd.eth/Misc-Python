#!/usr/bin/python
# Filename: POC-SNMP-GET.py
# -*- coding: utf-8 -*-

import subprocess
import re

# To configure SNMP:
# 1. Install net-snmp
# 2. Put Avaya phone MIB (16xxmib12) in this dir C:\usr\share\snmp\mibs\
# 3. Add mibs Avaya-16xxIPTelephone-MIB to the end of snmp.conf

# SNMP Settings
Community_String = 'steinsgate'

# SNMP SSON / Object ID
SNMP_MAC = 'Avaya-16xxIPTelephone-MIB::ipEndpointMIBs.6.1.50.0'
SNMP_Serial = 'Avaya-16xxIPTelephone-MIB::ipEndpointMIBs.6.1.57.0'

SNMP_Firmware = 'Avaya-16xxIPTelephone-MIB::ipEndpointMIBs.6.1.4.0'
SNMP_IP = 'Avaya-16xxIPTelephone-MIB::ipEndpointMIBs.6.1.38.0'
SNMP_Model = 'Avaya-16xxIPTelephone-MIB::ipEndpointMIBs.6.1.52.0'

# Regex strings for SNMP output
Regex_MAC = '[0-9A-Fa-f]{12}|[0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}'

Regex_Serial = '\"(\w+)\"'
Regex_Firmware = '\w+.tar'
Regex_IP = '\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}'
Regex_Model = '9611G|9608'
Regex_ICMP_Reply = 'Reply from (\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})'

def SNMP_GET(IP='', Community_String=Community_String, Debug=False):
    ''' SNMP GET POC code '''
    # Try (1) SNMPGET to verify SNMP is enabled
    try:
        # Serial
        # SNMP GET
        SNMP_GET_Serial = subprocess.Popen(['snmpget', '-v', '2c', '-c', Community_String, str(IP), SNMP_Serial],\
                        stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        # SNMP results stored in to variable
        SNMP_Out_Serial, SNMP_Err_Serial = SNMP_GET_Serial.communicate()

        # Regex SNMP variable
        Serial = re.findall(Regex_Serial, SNMP_Out_Serial)[0]

    except:
        return # Exit function if SNMPGET MAC fails
        
    try:
        # MAC
        # SNMP GET
        SNMP_GET_MAC = subprocess.Popen(['snmpget', '-v', '2c', '-c', Community_String, str(IP), SNMP_MAC],\
                        stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        
        # SNMP results stored in to variable
        SNMP_Out_MAC, SNMP_Err_MAC = SNMP_GET_MAC.communicate()
        
        # Regex SNMP variable, replace colons, uppercase characters
        MAC = re.findall(Regex_MAC, SNMP_Out_MAC)[0].replace(':', '').upper()

        # Firmware
        # SNMP GET
        SNMP_GET_Firmware = subprocess.Popen(['snmpget', '-v', '2c', '-c', Community_String, str(IP), SNMP_Firmware],\
                        stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        
        # SNMP results stored in to variable
        SNMP_Out_Firmware, SNMP_Err_Firmware = SNMP_GET_Firmware.communicate()

        # Regex SNMP variable
        Firmware = re.findall(Regex_Firmware, SNMP_Out_Firmware)[0]


        # Model
        # SNMP GET
        SNMP_GET_Model = subprocess.Popen(['snmpget', '-v', '2c', '-c', Community_String, str(IP), SNMP_Model],\
                        stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        
        # SNMP results stored in to variable
        SNMP_Out_Model, SNMP_Err_Model = SNMP_GET_Model.communicate()

        # Regex SNMP variable
        Model = re.findall(Regex_Model, SNMP_Out_Model)[0]


        # Time finished
        Date_and_Time = str(Date.datetime.now()).split(' ')

        # [MAC, SN, IP, FW, Model, Part_Number, TS, TE, Date]
        return [MAC, Serial, IP, Firmware, Model, 'PN', 'TS', Date_and_Time[1], Date_and_Time[0]]
    
    except:
        if Debug == True:
            print '\n[!] Got error during SNMP GET on', str(IP)


#print SNMP_GET('192.168.40.11')
