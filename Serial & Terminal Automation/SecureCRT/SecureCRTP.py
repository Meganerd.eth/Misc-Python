# $language = "python"
# $interface = "1.0"

#########################################################
''' IMPORTS '''

import SecureCRT
import sys
import re
import os
#from datetime import datetime
import datetime as Date
import random
import time


#########################################################
crt = None

def Init(obj):
    ''' Module import workaround for SecureCRT
    This allows us to import all of this code from another script '''
    global crt
    crt = obj
    return

# NOTE: If you modify this file, close and re-open SCRT! It caches this file in RAM so your changes wont run!

#########################################################
''' NOTES / SAMPLES / TIPS '''

# Dialogue prompt with Date
#Input_IOS = crt.Dialog.Prompt('Enter IOS: ', 'Date: ' + Time)


# Screen = crt.Screen.Get(-1, 1, msgboxwidth, msgboxheight) ?
# Screen = crt.Screen.Get(-1, 1, 100, 100)

# x = crt.Screen.WaitForString('#', Time_To_Wait)
# x = crt.Screen.WaitForString('#', 10)

# x =  crt.Dialog.Prompt('Input string:')
# crt.Dialog.MessageBox(x)

# no ip domain lookup
# ip domain lookup


# crt.Screen.SendSpecial('TN_BREAK')


# Cisco IOS modes:
#    Hostname> is Exec Mode
#    Hostname# is Privileged EXEC Mode
#    Hostname(config) is Global Configuration Mode
#    Hostname(config-if) is Interface Configuration Mode


#########################################################
''' MISC / GLOBALS / OTHER VARIABLES '''


# Types of prompts we could see
Prompts = ['#', '>', '(config-if)', '(config)']

# Regex patterns for prompts
# This will match all prompts and capture hostnames
Prompt_Re = '\S+(>|#|\((config-if|config)\))'


#LOG_FILE_TEMPLATE = os.path.join(LOG_DIRECTORY, "Command_%(NUM)s_Results.txt")

BANNED_CHARACTERS = ['%', '$', '*', '<', '>', '^', '\'', " ' ", '/', '?', '{', '}', '|', '"', '[', ']']

Timestamp = str(Date.datetime.now()) # YYYY-MM-DD HH:MM:SS.sssss
Date = Timestamp.split(' ')[0] # YYYY-MM-DD
Time = Timestamp.split(' ')[1].split('.')[0] # HH:MM:SS

# Logging settings
LOG_DIRECTORY = 'C:\\Logs\\'+str(Date)+'\\'

#########################################################

'''

Function: Action_Menu
Function status: Not Ready
Notes: Creating a menu that offers common scenarios, currently being built.

'''

def Action_Menu(Action=None, Debug=False):
    ''' Open dialogue with entry fields for actions to be performed
    Currently not completed *** '''
    objTab = crt.GetScriptTab()
    objTab.Screen.Synchronous = True
    objTab.Screen.IgnoreEscape = True

    if Action == None:
        Action = crt.Dialog.Prompt('''Enter action to perform

        1) Copy IOS from TFTP, reload, data capture Cisco router/switch

        2) Data capture Cisco router/switch (sh ver, dir, sh system, sh inv,
                                          sh lic, show ip int brief, sh run)

        3) Recover ROMMON mode Cisco router/switch

        4) Password recovery Cisco router/switch

        ''', 'Date: ' + Time)

    Login()
    Run('term len 0')

    if str(Action) == '1':
        if Debug == True:
            crt.Dialog.MessageBox('Option 1 entered')

        Login()
        objTab.Screen.Send('en\r\n')
        objTab.Screen.Send('term len 0\r\n')

        #Set_Mode(Go_To_Mode='')

        Current_Mode = Get_Mode() # [Mode, Prompt, Hostname]
        Hostname = Get_Hostname()
        Hostname_And_Prompt = Hostname+Current_Mode[1]


        Model = Get_Model()
        if not Model == '':
            IOS_To_Load = Ask_IOS(Model=Model)
            Copy_TFTP(Filename=IOS_To_Load, TFTP_Server_IP='10.200.2.8')
            Delete_Old_IOS(IOS_To_Keep=IOS_To_Load)

        objTab.Screen.Send('\r\n')
        objTab.Screen.Send('\r\n')
        time.sleep(5)

        Login()

        objTab.Screen.Send('en\r\n')
        objTab.Screen.Send('term len 0\r\n')

        Commands = ['show version', 'dir', 'show system', 'show inventory', 'show license', 'show ip int brief', 'show run']

        DCAP(Commands)

    elif str(Action) == '2':
        if Debug == True:
            crt.Dialog.MessageBox('Option 2 entered')

        objTab.Screen.Send('\r\n')
        objTab.Screen.Send('\r\n')
        time.sleep(5)

        Login()

        objTab.Screen.Send('en\r\n')
        objTab.Screen.Send('term len 0\r\n')

        Commands = ['show version', 'dir', 'show system', 'show inventory', 'show license', 'show ip int brief', 'show run']

        DCAP(Commands)


    elif str(Action) == '3':
        # Recover ROMMON mode Cisco router/switch
        if Debug == True:
            crt.Dialog.MessageBox('Option 3 entered')


        # Rommon 1 > confreg 0x2142

        # Is unit up?
        # Please reset unit
        crt.Dialog.MessageBox('Reset unit now')

        # Send Ctrl+c (break)
        for i in range(0, 9001):
            ROMMON = objTab.Screen.WaitForString('rommon 1 >', 1)
            if str(ROMMON) == '1':
                break
            crt.Dialog.MessageBox(str(ROMMON))
            crt.Screen.SendSpecial('TN_BREAK')

        # We should be in ROMMON mode now


        objTab.Screen.Send('confreg 0x2142\r\n')
        objTab.Screen.Send('reset\r\n')


        # Router#configure terminal
        # Enter configuration commands, one per line. End with CNTL/Z.
        # Router(config)#config-register 0x2102




    elif str(Action) == '4':
        # Password recovery Cisco router/switch
        if Debug == True:
            crt.Dialog.MessageBox('Option 4 entered')




#########################################################


def Ask_IOS(Model=None):
    objTab = crt.GetScriptTab()
    objTab.Screen.Synchronous = True
    objTab.Screen.IgnoreEscape = True

    # Check mode

    # Go to enable mode
    objTab.Screen.Send('en\r\n')

    # What model are we?
    if Model == None:
        Model = '1941'

    # Get common IOS
    Common_IOS_Files = Common_IOS(Model=Model)

    Prompt = '''What IOS?
    0) Enter IOS filename
    '''

    for n in range(0, len(Common_IOS_Files)):
        if n == 0:
            Prompt = Prompt+str(n+1)+') '+Common_IOS_Files[n]+'\n'
        else:
            Prompt = Prompt+'    '+str(n+1)+') '+Common_IOS_Files[n]+'\n'

    #Action = crt.Dialog.Prompt('''What IOS?
    #0) Enter IOS filename
    #
    #1) c1900-universalk9-mz.SPA.153-3.M6.bin
    #
    #2) c1900-universalk9-mz.SPA.155-3.M.bin
    #
    #''', 'What IOS?')

    Action = crt.Dialog.Prompt(Prompt, 'What IOS?')


    if str(Action) == '0':
        # else ask user to enter IOS
        Filename = crt.Dialog.Prompt('Input IOS Filename:')
    elif str(Action) == '1':
        Filename = Common_IOS_Files[int(Action)-1]
    elif str(Action) == '2':
        Filename = Common_IOS_Files[int(Action)-1]

    Alert(Message=Filename)
    return Filename


def Common_IOS(Model=''):
    if Model == '1941' or '1900':
        return ['c1900-universalk9-mz.SPA.155-3.M.bin', 'c1900-universalk9-mz.SPA.153-3.M6.bin']
    elif Model == '800':
        pass


#########################################################
''' RELOAD UNIT '''


'''

Function: Reload
Function status: Ready
Notes: This should successfully launch a reload on a Cisco Router/Switch
       Changes will be saved if Save_Changes are set to True; True is default
       This will also attempt to login once unit has reloaded

'''

def Reload(Save_Changes=True, Debug=False):
    ''' Reload unit, wait until unit reloads, then login '''
    objTab = crt.GetScriptTab()
    objTab.Screen.Synchronous = True
    objTab.Screen.IgnoreEscape = True

    Login()

    # Make sure we are at enable mode console
    objTab.Screen.Send('reload\r\n')


    if objTab.Screen.WaitForString('Proceed with reload', 10) == True:
        objTab.Screen.Send('\r\n')
        if Save_Changes == True:
            objTab.Screen.Send('yes\r\n')
        elif Save_Changes == False:
            objTab.Screen.Send('no\r\n')
        objTab.Screen.Send('\r\n')
        objTab.Screen.Send('\r\n')
        objTab.Screen.Send('\r\n')
        objTab.Screen.Send('\r\n')


#     # Bug in reload WaitForString, temp fix for muliple cases
#     if objTab.Screen.WaitForString('modified') == True:
#         if Save_Changes == True:
#             objTab.Screen.Send('yes\r\n')
#             objTab.Screen.Send('\r\n')
#             objTab.Screen.Send('\r\n')
#             objTab.Screen.Send('\r\n')
#
#         elif Save_Changes == False:
#             objTab.Screen.Send('no\r\n')
#             objTab.Screen.Send('\r\n')
#             objTab.Screen.Send('\r\n')
#             objTab.Screen.Send('\r\n')
#
#     if objTab.Screen.WaitForString('Save?') == True:
#         if Save_Changes == True:
#             objTab.Screen.Send('yes\r\n')
#             objTab.Screen.Send('\r\n')
#             objTab.Screen.Send('\r\n')
#             objTab.Screen.Send('\r\n')
#
#         elif Save_Changes == False:
#             objTab.Screen.Send('no\r\n')
#             objTab.Screen.Send('\r\n')
#             objTab.Screen.Send('\r\n')
#             objTab.Screen.Send('\r\n')
#
#     if objTab.Screen.WaitForString('Please answer') == True:
#         if Save_Changes == True:
#             objTab.Screen.Send('yes\r\n')
#             objTab.Screen.Send('\r\n')
#             objTab.Screen.Send('\r\n')
#             objTab.Screen.Send('\r\n')
#
#         elif Save_Changes == False:
#             objTab.Screen.Send('no\r\n')
#             objTab.Screen.Send('\r\n')
#             objTab.Screen.Send('\r\n')
#             objTab.Screen.Send('\r\n')



    # Wait until unit is reloaded, then login

    Reload_Prompts_ = ['Press RETURN to get started', 'User Name:', 'Username:']
    if objTab.Screen.WaitForStrings(Reload_Prompts_) == True:
        objTab.Screen.Send('\r\n')



        Login_Prompts_ = ['User Name:', 'Username:']

        if objTab.Screen.WaitForStrings(Login_Prompts_) == True:
            objTab.Screen.Send('\r\n')
            if objTab.Screen.WaitForStrings(Login_Prompts_) == True:
                Login()


#########################################################
''' SET IP ADDRESS '''



'''

Function: Set_IP
Function status: Not Ready
Notes: This should successfully set an IPv4 address and mask (CIDR notation)
       on a specified interface. This interface can be fe, ge, or vlan.

       Error logging is enabled by default

'''


def Set_IP(IP_Address='dhcp', mask='255.255.255.0', interface='g0/0',
           Model=None, Device_Type=None, Log_Errors=True, Debug=False):
    ''' Set IPv4 address on unit
    Default is dhcp; otherwise specify IPv4 (eg: 192.168.10.1)
    Subnet mask default is class C (255.255.255.0)
    Default interface to assign IP address is g0/0
    Script will attempt to identify device type/model to automatically use the expected interface
    Model will be string of model
    Device_Type will be either router or switch '''
    objTab = crt.GetScriptTab()
    objTab.Screen.Synchronous = True
    objTab.Screen.IgnoreEscape = True

    # Check if switch -> then use vlan
    # If router -> use g0/0 or fe0/0


    objTab.Screen.Send('conf t\r\n')

    objTab.Screen.Send('int '+interface+'\r\n')

    # % Invalid input detected at '^' marker
    # Check for error on interface
    # Check if logging set to true
    # Log error
    if objTab.Screen.WaitForString('Invalid', 2) == True:
        if Debug == True:
            crt.Dialog.MessageBox('Bad syntax on iface: '+interface)
        #objTab.Screen.Send(Filename+'\r\n')


    if IP_Address.lower() == 'dhcp':
        objTab.Screen.Send('ip address dhcp\r\n')


    elif len(IP_Address.split('.')) == 4:
        pass


    objTab.Screen.Send('no shut\r\n')
    objTab.Screen.Send('end\r\n') # Return to Privileged EXEC Mode


# objTab.Screen.Send('conf t\r\n')
# objTab.Screen.Send('int g0/0\r\n')
# #objTab.Screen.Send('ip address 10.48.16.71 255.255.255.0\r\n')
# objTab.Screen.Send('ip address dhcp\r\n')
# objTab.Screen.Send('no shut\r\n')
# objTab.Screen.Send('end\r\n')
# #objTab.Screen.Send('ip route 0.0.0.0 0.0.0.0 10.48.16.1\r\n')




#########################################################

def Copy_To_Startup_Config(Filename=None, DstFilename='startup-config', Path='flash:',
                           Reload=False, Debug=False):
    ''' Copy <Filename> config file from flash to startup-config
    Reload unit if boolean set to True
    Path by default is flash: '''
    objTab = crt.GetScriptTab()
    objTab.Screen.Synchronous = True
    objTab.Screen.IgnoreEscape = True

    if Filename == None or Filename == '':
        Filename = crt.Dialog.Prompt('Enter filename to copy to '+DstFilename, 'Date: ' + Time)

    objTab.Screen.Send('copy '+Filename+' '+DstFilename+'\r\n')

    if objTab.Screen.WaitForString('Destination', 2) == True:
        objTab.Screen.Send(Filename+'\r\n')

    if objTab.Screen.WaitForString('File not found', 2) == True:
        crt.Dialog.MessageBox('Config file: '+Filename+' not found on TFTP server')

    else: # File found, copied successfully; write to memory
        Run('wr mem')

    if Reload == True:
        Reload()



#Copy_To_Startup_Config()

#########################################################

def Delete_Old_IOS(IOS_To_Keep='', Path='flash:', Debug=False):
    ''' Delete old IOS once we are done copying new one to unit
    if IOS_To_Keep is unspecified we will keep the latest one '''
    objTab = crt.GetScriptTab()
    objTab.Screen.Synchronous = True
    objTab.Screen.IgnoreEscape = True
    Login()
    Dir_Output = Run('dir')

    if Debug == True:
        crt.Dialog.MessageBox(Dir_Output)

    # If unspecified, delete oldest IOS here
    # Will finish later ***

    # Get all IOS from Dir
    All_IOS = re.findall('(c.+\.bin)', Dir_Output)

    # Check for IOS we wan in Dir before removing any, to prevent ROMMON
    # c1900-universalk9-mz.SPA.153-3.M6.bin
    Valid_IOS = re.findall('('+IOS_To_Keep+')', Dir_Output)

    # if Valid_IOS matched carry on
    if len(Valid_IOS) > 0:
        Valid_IOS = Valid_IOS[0]

    for IOS in All_IOS:
        if not IOS == Valid_IOS:
            #Delete old IOS file
            objTab.Screen.Send('delete flash:'+IOS+'\r\n')
            objTab.Screen.Send('\r\n')
            objTab.Screen.Send('\r\n')







#########################################################

def Get_Mode(Debug=False):
    ''' Identify and return the mode the router is currently in
    return will be [Mode, Prompt, Hostname]
    if error on getting current mode return will be [None, None, Hostname]
    Hostname> is Exec Mode
    Hostname# is Privileged EXEC Mode
    Hostname(Config) is Global Configuration Mode
    Hostname(Config-if) is Interface Configuration Mode '''

    objTab = crt.GetScriptTab()
    objTab.Screen.Synchronous = True
    objTab.Screen.IgnoreEscape = True

    Prompt_Found = '' # Initial value
    Hostname = '' # Initial value

    # Send blank line
    objTab.Screen.Send('\r\n')

    # check for required login just in case
#     if objTab.Screen.WaitForString('Press RETURN to get started', 2) == True:
#         objTab.Screen.Send('\r\n')
#         time.sleep(5)
#         objTab.Screen.Send(Username+'\r\n')
#         objTab.Screen.WaitForString('Password', 2)
#         objTab.Screen.Send(Password+'\r\n')
#         time.sleep(5)
#
#     if objTab.Screen.WaitForString('Username:', 1) == True:
#         objTab.Screen.Send(Username+'\r\n')
#         objTab.Screen.WaitForString('Password', 1)
#         objTab.Screen.Send(Password+'\r\n')
#         time.sleep(1)
#
#     if objTab.Screen.WaitForString('User Name:', 1) == True:
#         objTab.Screen.Send(Username+'\r\n')
#         objTab.Screen.WaitForString('Password', 1)
#         objTab.Screen.Send(Password+'\r\n')
#         time.sleep(1)

    # Send blank line
    objTab.Screen.Send('\r\n')

    # Get hostname; to assist in getting mode
    Hostname = objTab.Screen.ReadString(Prompts)
    Hostname_len = len(Hostname)

    # Look at blank lines for which prompt we have
    #Screen = crt.Screen.Get(-1, 1, 100, 100)
    Screen = crt.Screen.Get(-1, Hostname_len-1, 100, 100)

    if Debug == True:
        crt.Dialog.MessageBox('Hostname: '+Hostname)
        #crt.Dialog.MessageBox('Output: '+Screen)

    # Split output to list; then reverse list
    # We want the most recent output
    Output = Screen.split(' ')[::-1]

    # Iterate & parse for most recent output; this will be our prompt
    for i in Output:
        if len(i) > 0:
            if Debug == True:
                crt.Dialog.MessageBox('Prompt found: '+i)
            if Debug == True:
                crt.Dialog.MessageBox('Prompt found: '+i)
            Prompt_Found = i
            break

    # Identify which prompt this is; return this info
    if Prompt_Found == '#':
        # Enable mode == privileged EXEC Mode
        return ['Privileged EXEC', Prompt_Found, Hostname]

    elif Prompt_Found == '(config)' or Prompt_Found == '(config)#':
        # (config) == Global Configuration Mode
        return ['Global Configuration', Prompt_Found, Hostname]

    elif Prompt_Found == '(config-if)' or Prompt_Found == '(config-if)#':
        # (config-if) == Interface Configuration Mode
        return ['Interface Configuration', Prompt_Found, Hostname]

    elif Prompt_Found == '>':
        # > == EXEC or rommon Mode
        # Identify EXEC or ROMMON mode, since both prompts are same
        if Hostname.lower() == 'rommon':
            return ['rommon', Prompt_Found, Hostname]

        else:
            return ['EXEC', Prompt_Found, Hostname]

    else:
        return [None, None, Hostname]


#Get_Mode()

#########################################################
''' GO TO USER EXEC MODE '''
# Hostname>
#Set_Mode(Go_To_Mode='EXEC')

#########################################################
''' GO TO PRIVILEGED EXEC MODE '''
# Hostname#
#Set_Mode(Go_To_Mode='')

#########################################################
''' GO TO GLOBAL CONFIGURATION MODE '''
# Hostname(config)
#Set_Mode(Go_To_Mode='')

#########################################################
''' GO TO INTERFACE CONFIGURATION MODE '''
# Hostname(config-if)


#########################################################
''' GO TO ROMMON MONITOR MODE '''
# ROMMON>


#########################################################
''' MOVE FROM CURRENT MODE TO DESIRED CONFIGURATION MODE '''

def Set_Mode(Go_To_Mode='', Interface=None, Debug=False):
    ''' Switch mode on router/switch
    Go_To_Mode should  be EXEC, Privileged EXEC
    Case-sensitivity does not matter for mode input *** a few strings need fixed
    Alternatively you can input: >, #, or (config)
    You cannot enter (config-if) in this function currently, but you can exit this mode ***
    Interface parameter should be specified if Go_To_Mode == Interface Configuration
    Hostname> is Exec Mode
    Hostname# is Privileged EXEC Mode
    Hostname(config) is Global Configuration Mode
    Hostname(config-if) is Interface Configuration Mode '''

    objTab = crt.GetScriptTab()
    objTab.Screen.Synchronous = True
    objTab.Screen.IgnoreEscape = True

    if Go_To_Mode == '>':
        Go_To_Mode = 'Exec'

    elif Go_To_Mode == '#':
        Go_To_Mode = 'Privileged EXEC'

    elif Go_To_Mode == '(config)':
        Go_To_Mode = 'Global Configuration'

    elif Go_To_Mode == '(config-if)':
        if not Interface == None:
            Go_To_Mode = 'Interface Configuration'
        elif Interface == None:
            # User will need to specify interface they want to go to
            pass # Unfinished ***

    # Heirarchy of paths on modes we can move between
    # This list is not exhausted of all possibilities
    # rommon will not be on the list
    # >, #, (config), (config-if)
    Paths = ['EXEC',
             'Privileged EXEC',
             'Global Configuration',
             'Interface Configuration']

    # Get the current mode
    Current_Mode = Get_Mode() # [Mode, Prompt, Hostname]

    # Move to desired mode from current mode
    if Current_Mode[0].upper() == 'EXEC': # Hostname>
        if Go_To_Mode == 'Privileged EXEC': # Hostname#
            objTab.Screen.Send('en\r\n')
            New_Mode = Get_Mode() # Check mode now
            if New_Mode[0] == 'Privileged EXEC':
                pass # We good yo
            else:
                # We can try to move from where we are, but this shouldnt happen
                # Finish later ***
                pass

        elif Go_To_Mode == 'Global Configuration': # Hostname(config)
            objTab.Screen.Send('en\r\n')
            objTab.Screen.Send('conf t\r\n')
            New_Mode = Get_Mode() # Check mode now
            if New_Mode[0] == 'Global Configuration':
                pass # We good yo
            else:
                # We can try to move from where we are, but this shouldnt happen
                # Finish later ***
                pass

    elif Current_Mode[0] == 'Privileged EXEC': # Hostname#
        if Go_To_Mode.upper() == 'Exec': # Hostname>
            objTab.Screen.Send('exit\r\n')
            New_Mode = Get_Mode() # Check mode now
            if New_Mode[0] == 'Exec':
                pass # We good yo
            else:
                # We can end up here in this case
                # We may not be able to enter Exec mode on some units
                # Will need testing to debug this scenario ***
                if Debug == True:
                    crt.Dialog.MessageBox('Unable to enter EXEC Mode\r\n(eg: Hostname>)')

        elif Go_To_Mode == 'Global Configuration': # Hostname(config)
            objTab.Screen.Send('conf t\r\n')
            New_Mode = Get_Mode() # Check mode now
            if New_Mode[0] == 'Global Configuration':
                pass # We good yo
            else:
                # We can try to move from where we are, but this shouldnt happen
                # Finish later ***
                pass

    elif Current_Mode[0] == 'Global Configuration': # Hostname(config)
        if Go_To_Mode.upper() == 'Exec': # Hostname>
            objTab.Screen.Send('exit\r\n')
            objTab.Screen.Send('exit\r\n')
            New_Mode = Get_Mode() # Check mode now
            if New_Mode[0] == 'Exec':
                pass # We good yo
            else:
                # We can end up here in this case
                # We may not be able to enter Exec mode on some units
                # Will need testing to debug this scenario ***
                if Debug == True:
                    crt.Dialog.MessageBox('Unable to enter EXEC Mode\r\n(eg: Hostname>)')

        elif Go_To_Mode == 'Privileged EXEC': # Hostname#
            objTab.Screen.Send('exit\r\n')
            New_Mode = Get_Mode() # Check mode now
            if New_Mode[0] == 'Privileged EXEC':
                pass # We good yo
            else:
                # We can try to move from where we are, but this shouldnt happen
                # Finish later ***
                pass

    elif Current_Mode[0] == 'Interface Configuration': # Hostname(config-if)
        if Go_To_Mode == 'Privileged EXEC': # Hostname#
            objTab.Screen.Send('exit\r\n')
            objTab.Screen.Send('exit\r\n')
            New_Mode = Get_Mode() # Check mode now
            if New_Mode[0] == 'Privileged EXEC':
                pass # We good yo
            else:
                # We can try to move from where we are, but this shouldnt happen
                # Finish later ***
                pass

        elif Go_To_Mode == 'Global Configuration': # Hostname(config)
            objTab.Screen.Send('exit\r\n')
            New_Mode = Get_Mode() # Check mode now
            if New_Mode[0] == 'Global Configuration':
                pass # We good yo
            else:
                # We can try to move from where we are, but this shouldnt happen
                # Finish later ***
                pass

    else: # If we cannot identify the mode for some reason alert user
        crt.Dialog.MessageBox('Unable to identify current mode!')




#Set_Mode(Go_To_Mode='#')
#Set_Mode(Go_To_Mode='(config)')


#########################################################
''' LOGIN TO UNIT '''


def Login(Username='cisco', Password='cisco', Debug=False):
    ''' Login to the switch/router
    ignores login if not at login screen '''
    objTab = crt.GetScriptTab()
    objTab.Screen.Synchronous = True
    objTab.Screen.IgnoreEscape = True

    objTab.Screen.Send('\r\n')

    Login_Prompts = ['Would you like to terminate autoinstall',
                     'Would you like to enter the initial configuration dialog',
                     'Press RETURN to get started',
                     'Username:',
                     'User Name:',
                     'Password:',
                     'Switch>',
                     'yourname>',
                     'Router>',
                     'Router#',
                     'authentication failed',
                     'speed',
                      '#',
                      '>']


    Login = objTab.Screen.WaitForStrings(Login_Prompts)

    if Login == 1:
        if Debug == True:
            crt.Dialog.MessageBox('Case 1 on Login prompts')
        objTab.Screen.Send('\r\n')
        Login(Username, Password, Debug)

    elif Login == 2:
        if Debug == True:
            crt.Dialog.MessageBox('Case 2 on Login prompts')
        objTab.Screen.Send('\r\n')
        Login(Username, Password, Debug)

    elif Login == 3:
        if Debug == True:
            crt.Dialog.MessageBox('Case 3 on Login prompts')
        objTab.Screen.Send('\r\n')
        time.sleep(5)
        Login(Username, Password, Debug)

    elif Login == 4:
        if Debug == True:
            crt.Dialog.MessageBox('Case 4 on Login prompts')
        objTab.Screen.Send(Username+'\r\n')
        objTab.Screen.WaitForString('Password', 2)
        objTab.Screen.Send(Password+'\r\n')

    elif Login == 5:
        if Debug == True:
            crt.Dialog.MessageBox('Case 5 on Login prompts')
        objTab.Screen.Send(Username+'\r\n')
        objTab.Screen.WaitForStrings('Pass')
        objTab.Screen.Send(Password+'\r\n')

    elif Login == 6:
        if Debug == True:
            crt.Dialog.MessageBox('Case 6 on Login prompts')

    elif Login == 7:
        if Debug == True:
            crt.Dialog.MessageBox('Case 7 on Login prompts')
        objTab.Screen.Send('en\r\n')
    elif Login == 8:
        if Debug == True:
            crt.Dialog.MessageBox('Case 8 on Login prompts')
        objTab.Screen.Send('en\r\n')
    elif Login == 9:
        if Debug == True:
            crt.Dialog.MessageBox('Case 9 on Login prompts')
        objTab.Screen.Send('en\r\n')
    elif Login == 10:
        if Debug == True:
            crt.Dialog.MessageBox('Case 10 on Login prompts')

    elif Login == 11:
        if Debug == True:
            crt.Dialog.MessageBox('Case 11 on Login prompts')

    elif Login == 12:
        if Debug == True:
            crt.Dialog.MessageBox('Case 12 on Login prompts')

    elif Login == 13:
        if Debug == True:
            crt.Dialog.MessageBox('Case 13 on Login prompt')

    elif Login == 14:
        if Debug == True:
            crt.Dialog.MessageBox('Case 14 on Login prompt')
        objTab.Screen.Send('en\r\n')

    else:
        if Debug == True:
            crt.Dialog.MessageBox('No Login prompts found')
        objTab.Screen.Send('\r\n')


#Login()


#########################################################

def Get_Model(Debug=False):
    ''' Get Model of router-switch '''
    crt.Screen.Send('show version | i Cisco IOS' + chr(13))
    crt.Screen.WaitForString('#')
    Sleep(Seconds=1)
    Screen = crt.Screen.Get(-1, 1, 100, 100)
    Model_Text = Screen.split(' ')

    #for element in Model_Text:
    #    element = element.encode('ascii','ignore')

    Model = ''
    Models = ['2951', '2921', '2911', '2901', '3845', \
              '3825', '3945', '3945E', '3925', '3925E', \
              'ASR 1013', 'ASR 1006', 'ASR 1004', 'ASR 1002', \
              'ASR 1001', 'ASR 1002-X', 'ASR 1001-X', \
              '7201', '7206VXR', '1941', '1941W', '1921', \
              '1905', '7304', '7301', '881', '800', '891', \
              '1905', '2600', '1900', '2900', '1941']

    for element in Models:
        if element in Screen:
            Model = element

    for i in range(0, 5):
        if Model == '':
            # Try again, CLI may have been lagging
            # But it would be annoying to initially assume this with a delay
            Sleep(Seconds=1)
            Screen = crt.Screen.Get(-1, 1, 100, 100)
            for element in Models:
                if element in Screen:
                    Model = element
        else:
            break

    #crt.Dialog.MessageBox(Screen)

    if Debug == True:
        crt.Dialog.MessageBox('Model: '+Model)
    return Model

#Get_Model(Debug=True)


#########################################################

def Get_Serial(Debug=False):
    ''' Get serial of router-switch '''
    objTab = crt.GetScriptTab()
    objTab.Screen.Synchronous = True
    objTab.Screen.IgnoreEscape = True
    #Regex_Router_Serial = 'Processor board ID (.+){5,15}'
    szPrompt = '#'
    szCommand = str('show version | i Processor board ID')
    objTab.Screen.Send(szCommand + '\r\n')
    objTab.Screen.WaitForString(szCommand + '\r\n', 2)
    szResult = objTab.Screen.ReadString(szPrompt)
    Serial = szResult.split(' ')[3].split('\r\n')[0] # Parse output
    if Debug == True:
        crt.Dialog.MessageBox(Serial)
    return Serial

#Get_Serial()


#########################################################

def Get_Inventory(Hostname=None, Debug=False):
    ''' Get show inventory output of router-switch '''
    objTab = crt.GetScriptTab()
    objTab.Screen.Synchronous = True
    objTab.Screen.IgnoreEscape = True

    if Hostname_And_Prompt == None:
        # Go to fail
        pass

    #szPrompt = Hostname.replace('\n', '').replace('\r', '')
    szPrompt = Hostname.replace('\n', '').replace('\r', '')+'#'
    szCommand = str('show inventory')
    objTab.Screen.Send(szCommand + '\r\n')
    objTab.Screen.WaitForString(szCommand + '\r\n', 2)
    szResult = objTab.Screen.ReadString(szPrompt)
    #crt.Dialog.MessageBox(szResult)
    return str(szResult)


#Get_Inventory()

#########################################################


def Get_All_IOS(Debug=False):
    ''' return all IOS on system from directory output
    return will be list (eg: [1.5.3.3.bin, 1.5.3.3.m6.bin,..] '''
    objTab = crt.GetScriptTab()
    objTab.Screen.Synchronous = True
    objTab.Screen.IgnoreEscape = True
    Login()
    Dir_Output = Run('dir')

    # Get all IOS from Dir output
    All_IOS = re.findall('(c.+\.bin)', Dir_Output)

    if Debug == True:
        crt.Dialog.MessageBox(str(All_IOS))

    return All_IOS




#########################################################

def Get_Hostname(Debug=False):
    ''' Get and return hostname from unit '''
    objTab = crt.GetScriptTab()
    objTab.Screen.Synchronous = True
    objTab.Screen.IgnoreEscape = True
    objTab.Screen.Send('\r\n')

    # This is a blank regex pattern intentionally
    # SecureCRT API appears to require some real command to capture hostname
    #objTab.Screen.Send('sh ver | i Regex_Hostname')

    # This method is actually slower, even though the parsing is easier
    # Even though we use regex the unit will still run the full command
    objTab.Screen.Send('sh run | i hostname')

    objTab.Screen.Send('\r\n')
    szResult = objTab.Screen.ReadString(Prompts)
    #Hostname = re.findall(Prompt_Re, szResult)
    if Debug == True:
        crt.Dialog.MessageBox(szResult)

    #crt.Dialog.MessageBox(szResult.split('\r\n')[1])
    return str(szResult.split('\r\n')[1])


#Get_Hostname(True)

#########################################################

def Copy_TFTP(Filename=None, TFTP_Server_IP=None, DstFileSystem='flash:',
              DstFileName=None, Notes='', Debug=False):
    ''' Copy <file> from TFTP to Flash: or other specified dest path
    Filename assumed path is specified if file is not in TFTP root
    returns boolean; True on success
    Notes are specific notes you wish to include while asking user which file to copy '''
    objTab = crt.GetScriptTab()
    objTab.Screen.Synchronous = True
    objTab.Screen.IgnoreEscape = True

    if Filename == None or Filename == '':
        if not Notes == '': # Include notes to ask user
            Filename =  crt.Dialog.Prompt('(Notes: '+Notes+')\r\n\r\nInput Filename to copy from TFTP:')

        elif Notes == '': # No notes to add
            Filename =  crt.Dialog.Prompt('Input Filename to copy from TFTP:')

    if TFTP_Server_IP == None or TFTP_Server_IP == '':
        TFTP_Server_IP =  crt.Dialog.Prompt('Input TFTP Server IP:')

    if DstFileName == None or DstFileName == '':
        DstFileName = Filename

    # Check current mode
    Current_Mode = Get_Mode() # [Mode, Prompt, Hostname]

    if Current_Mode[0] == 'Privileged EXEC': # Hostname#
        pass # This is the correct mode

    elif Current_Mode[0] == 'Global Configuration': # Hostname(config)
        objTab.Screen.Send('end\r\n')

    elif Current_Mode[0] == 'Interface Configuration': # Hostname(config-if)
        objTab.Screen.Send('end\r\n')

    elif Current_Mode[0] == 'Exec Mode': # Hostname>
        objTab.Screen.Send('en\r\n')
        # Authenticate if asked
        if objTab.Screen.WaitForString('pass', 2) == True:
            # Try cisco as password
            objTab.Screen.Send('cisco\r\n')
            # On fail ask user
            if objTab.Screen.WaitForString('pass', 2) == True:
                Password_ = crt.Dialog.Prompt('''Enter the password to enter Privileged EXEC Mode''', 'Enter password')
                objTab.Screen.Send(Password_+'\r\n')

    # We should now be at Privileged EXEC Mode

    # Start tftp copy
    objTab.Screen.Send('copy tftp://'+TFTP_Server_IP+'/'+Filename+' '+DstFileSystem+DstFileName+'\r\n')

    # Confirm filename dialogue
    if crt.Screen.WaitForString('Destination filename') == True:
        objTab.Screen.Send(DstFileName+'\r\n') # Redundant, but enter filename again

#     if crt.Screen.WaitForString('Error') == True:
#         crt.Dialog.MessageBox('Copy TFTP command got an error')
#         return False
#     else:
#         return True



#Copy_TFTP(Filename='c1900-universalk9-mz.SPA.153-3.M6.bin',
#          TFTP_Server_IP='10.48.1.50', Debug=True)

#########################################################

def Get_IOS(IOS=None, TFTP_Server_IP=None, DstFileSystem='flash:', DstFileName=None):
    ''' Get IOS from TFTP '''
    objTab = crt.GetScriptTab()
    objTab.Screen.Synchronous = True
    objTab.Screen.IgnoreEscape = True

    if IOS == None:
        IOS =  crt.Dialog.Prompt('Input IOS filename:')

    if TFTP_Server_IP == None:
        TFTP_Server_IP =  crt.Dialog.Prompt('Input TFTP Server IP:')

    objTab.Screen.Send('copy tftp '+str(DstFileSystem)+'\r\n')
    objTab.Screen.WaitForString('Address or name of remote host', 2)
    objTab.Screen.Send(TFTP_Server_IP+'\r\n')
    objTab.Screen.WaitForString('Source filename', 2)
    objTab.Screen.Send(IOS+'\r\n')
    objTab.Screen.WaitForString('Destination filename', 2)

    if DstFileName == None:
        objTab.Screen.Send(IOS+'\r\n')
    else:
        objTab.Screen.Send(DstFileName+'\r\n')

#Get_IOS()




#########################################################


def Dir(Debug=False):
    ''' Return a directory listing '''
    Dir_Output = Run('dir')
    if Debug == True:
        crt.Dialog.MessageBox(Dir_Output)


#Dir()

#########################################################

def Run(Command='', Debug=False):
    ''' Run <Parameter|Command> and return output '''
    objTab = crt.GetScriptTab()
    objTab.Screen.Synchronous = True
    objTab.Screen.IgnoreEscape = True

    if 'sh' in Command.lower() and 'inv' in Command.lower():
        Hostname = Get_Hostname()
        Inventory = Get_Inventory(Hostname=Hostname)
        return str(Inventory)

    # Get prompt
    szPrompt = '#'

    #Prompts = ['#', '>', '(config)', '(config-if)']
    Prompts_ = ['#\r\n', '>\r\n', '(config)\r\n', '(config-if)\r\n'] # Should we add \r\n?; Unsure ***

    szCommand = str(Command)

    objTab.Screen.Send(szCommand + '\r\n')

    objTab.Screen.WaitForString(szCommand + '\r\n')

    ##objTab.Screen.WaitForStrings(Prompts_)

    szResult = objTab.Screen.ReadString(szPrompt)

    if Debug == True:
        crt.Dialog.MessageBox(szResult)

    return szResult

#Run(Command='sh ver', Debug=True)

#Run('dir flash:')

#########################################################

def Sleep(Seconds=5):
    ''' Sleep for N seconds '''
    objTab = crt.GetScriptTab()
    objTab.Screen.Synchronous = True
    objTab.Screen.IgnoreEscape = True
    objTab.Screen.WaitForString('NULLSTRING', int(Seconds))
    return

#########################################################

def Alert(Message=''):
    ''' Pop dialogue for user '''
    objTab = crt.GetScriptTab()
    objTab.Screen.Synchronous = True
    objTab.Screen.IgnoreEscape = True
    crt.Dialog.MessageBox(Message)
    return



#########################################################

def Set_Term_Len(len='0'):
    ''' Set terminal buffer length '''
    objTab = crt.GetScriptTab()
    objTab.Screen.Synchronous = True
    objTab.Screen.IgnoreEscape = True
    szPrompt = '#'
    szCommand = str('term len '+len)
    objTab.Screen.Send(szCommand + '\r\n')
    objTab.Screen.WaitForString(szCommand + '\r\n', 2)

#########################################################

def LaunchViewer(filename):
    ''' Opens a file '''
    try:
        os.startfile(filename)
    except AttributeError:
        subprocess.call(['open', filename])

#########################################################

def Log(Commands=[], Hostname=None):
    ''' Run Commands and log to file '''
    objTab = crt.GetScriptTab()
    objTab.Screen.IgnoreEscape = True
    objTab.Screen.Synchronous = True
    # If input is only string, convert to tuple
    if type(Commands) == str:
        x = Commands
        Commands = []
        Commands.append(x)

    # If hostname isnt specified then we will
    # use the commands for the filename string
    if Hostname == None:
        try:
            Hostname = 'Null_'+str(random.randint(1,1000000000000))+str(Date.datetime.now())
        except:
            Hostname = 'Null_'
        # Bug when weird command input
        #Cmds = Commands
        #for i in Cmds:
        #    i = str(i)
        #Hostname = '_'.join(Cmds)


    if not os.path.exists(LOG_DIRECTORY):
        os.mkdir(LOG_DIRECTORY)

    if not os.path.isdir(LOG_DIRECTORY):
        crt.Dialog.MessageBox(
            'Log output directory %r is not a directory' % LOG_DIRECTORY)
        return

    if not objTab.Session.Connected:
        crt.Dialog.MessageBox(
            'Not Connected.  Please connect before running this script.')
        return

    while True:
        if not objTab.Screen.WaitForCursor(1):
            break

    rowIndex = objTab.Screen.CurrentRow
    colIndex = objTab.Screen.CurrentColumn - 1

    prompt = objTab.Screen.Get(rowIndex, 0, rowIndex, colIndex)
    prompt = prompt.strip()

    Filename = str(Hostname)+'.txt'

    for command in Commands:
        File = open(LOG_DIRECTORY+Filename, 'wb+')
        try:
            Output = str(Run(command))
            crt.Dialog.MessageBox(Output)
            File.write(Output)
            File.close()
        except:
            pass

    LaunchViewer(LOG_DIRECTORY)

#########################################################

def DCAP(Commands=[], Hostname=None):
    ''' Ready to go data capture function '''
    # If input is only string, convert to tuple
    if type(Commands) == str:
        x = Commands
        Commands = []
        Commands.append(x)

    # If hostname isnt specified then we will
    # use the commands for the filename string
    if Hostname == None:
        try:
            d = str(Date.datetime.now()).split(' ')[0]
            t = str(Date.datetime.now()).split(' ')[1].replace(':', '-')[0:8]

            Hostname = 'DCAP_'+str(random.randint(1,10000))#+'_Date-'+d+'_Time-'+t
        except:
            Hostname = 'DCAP_'+str(random.randint(1,10000))#+'_

    Filename = str(Hostname)+'.txt'

    #crt.Dialog.MessageBox(LOG_DIRECTORY+Filename)

    try:
        File = open(LOG_DIRECTORY+Filename, 'wb+')
    except:
        Name = 'DCAP_'+str(random.randint(1,100000000))+'.txt'
        File = open(LOG_DIRECTORY+Name, 'wb+')

    for Cmd in Commands:
        strOut = Run(str(Cmd))
        File.write('\r\n' * 5)
        File.write('==' * 25)
        File.write('\r\nCommand: '+str(Cmd)+'\r\n')
        File.write('--' * 25)
        File.write('\r\n' * 2)
        File.write(strOut)
        #time.sleep(1)

    File.close()

    LaunchViewer(LOG_DIRECTORY)

    return

#########################################################


#Timestamp = str(Date.datetime.now()) # YYYY-MM-DD HH:MM:SS.sssss
#Date = Timestamp.split(' ')[0] # YYYY-MM-DD
#Time = Timestamp.split(' ')[1].split('.')[0] # HH:MM:SS

def Log_Time_Taken():
    ''' Log the time to a CSV file . this file will be a daily report '''
    objTab = crt.GetScriptTab()
    objTab.Screen.Synchronous = True
    objTab.Screen.IgnoreEscape = True
    Now_Timestamp = str(Date.datetime.now()) # YYYY-MM-DD HH:MM:SS.sssss
    Date = Timestamp.split(' ')[0] # YYYY-MM-DD
    Time = Timestamp.split(' ')[1].split('.')[0] # HH:MM:SS



#########################################################

def Create_Directory(Directory, Debug=False):
    ''' Create specified directory on the OS '''
    objTab = crt.GetScriptTab()
    objTab.Screen.Synchronous = True
    objTab.Screen.IgnoreEscape = True
    if not os.path.exists(Directory):
        os.makedirs(Directory)
        if Debug == True:
            crt.Dialog.MessageBox('Directory '+Directory+' created')
    elif os.path.exists(Directory):
        if Debug == True:
            crt.Dialog.MessageBox('Directory '+Directory+' already exists')
    else:
        if Debug == True:
            crt.Dialog.MessageBox('Error creating directory: '+Directory)

#########################################################

def Check_File_Exists(filename, Debug=False):
    ''' Returns True if file exists, False if File does not exist.
    Input should be path+file '''
    objTab = crt.GetScriptTab()
    objTab.Screen.Synchronous = True
    objTab.Screen.IgnoreEscape = True
    if os.path.isfile(filename):
        if Debug == True:
            crt.Dialog.MessageBox('File '+filename+' already exists')
        return True
    else:
        if Debug == True:
            crt.Dialog.MessageBox('File '+filename+' does not exist')
        return False


#########################################################

def Check_Directory_Exists(Directory, Debug=False):
    ''' Returns True if directory exists, False if File does not exist. '''
    objTab = crt.GetScriptTab()
    objTab.Screen.Synchronous = True
    objTab.Screen.IgnoreEscape = True
    if os.path.isdir(Directory):
        if Debug == True:
            crt.Dialog.MessageBox('Directory '+Directory+' already exists')
        return True
    else:
        if Debug == True:
            crt.Dialog.MessageBox('Directory '+Directory+' does not exist')
        return False


#########################################################

def ROMMON_Password_Recovery(Debug=False):
    ''' From ROMMON sets register 0x2142, resets, set register back to 0x2102, erase startup-config '''
    objTab = crt.GetScriptTab()
    objTab.Screen.Synchronous = True
    objTab.Screen.IgnoreEscape = True

    # Get to ROMMON Mode
    # *** Manual for now
    # Send break here ^c

    # From here we should be in ROMMON Mode
    # Set register 0x2142
    objTab.Screen.WaitForString('rommon', 2)
    objTab.Screen.Send('confreg 0x2142'+'\r\n')

    # Reset
    objTab.Screen.WaitForString('rommon', 2)
    objTab.Screen.Send('reset'+'\r\n')

    # Let the unit reload

    # Login

    # Set 0x2102
    #config-register 0x2102

    # Erase startup-config





#########################################################
'''
OLD DEBUG SAMPLES
'''


#Get_Model(Debug=True)
#Get_Serial(Debug=True)
#Get_All_IOS(Debug=True)
#Get_Hostname(Debug=True)


#Copy_TFTP(Filename='c1900-universalk9-mz.SPA.153-3.M6.bin', TFTP_Server_IP=)
#Delete_Old_IOS(IOS_To_Keep='c1900-universalk9-mz.SPA.153-3.M6.bin')

#Reload(Save_Changes=True)


# sx300_fw-1424.ros
#ROMMON_Password_Recovery()



#Set_IP(interface='vlan 1', Debug=True)




#Get_Mode(Debug=True)

#Set_Mode(Go_To_Mode='#')
#Set_Mode(Go_To_Mode='(config)')



# objTab.Screen.Send('term len 0\r\n')
#
#
# crt.Dialog.MessageBox(Date)
#
# objTab.Screen.Send('copy tftp flash')
#
# objTab.Screen.Send('\r\n')
#
#
#
# if objTab.Screen.WaitForString('Address or name', 2) == True:
#     objTab.Screen.Send('10.48.1.50')
#     objTab.Screen.Send('\r\n')
#
#
# objTab.Screen.WaitForString('Destination')
#
#
# objTab.Screen.Send('startup-config')
#
# objTab.Screen.Send('\r\n')
#
#
#
#
# if objTab.Screen.WaitForString('Error', 2) == True:
#     crt.Dialog.MessageBox(Date)
#
#
#
# objTab.Screen.Send('\r\n')
# objTab.Screen.Send('\r\n')
# objTab.Screen.Send('\r\n')
# objTab.Screen.Send('\r\n')
#
#
#
# Delete_Old_IOS(IOS_To_Keep='c1900-universalk9-mz.SPA.153-3.M6.bin')



#
# def Test(Device='Router'):
#
#     objTab = crt.GetScriptTab()
#     objTab.Screen.IgnoreEscape = True
#     objTab.Screen.Synchronous = True
#
#     if Device == 'Router':
#         crt.Dialog.MessageBox('you input a router')
#         objTab.Screen.Send('conf t\r\n')
#         objTab.Screen.Send('int g0/1\r\n')
#         objTab.Screen.Send('ip address dhcp\r\n')
#         objTab.Screen.Send('no shut\r\n')
#         objTab.Screen.Send('end\r\n')
#
#     elif Device == 'Switch':
#         crt.Dialog.MessageBox('you input a router')
#         objTab.Screen.Send('conf t\r\n')
#         objTab.Screen.Send('vlan 1\r\n')
#         objTab.Screen.Send('ip address dhcp\r\n')
#         objTab.Screen.Send('no shut\r\n')
#         objTab.Screen.Send('end\r\n')
#
#
# Test()

#
#
#
#
# Login()
#
#
# objTab.Screen.Send('term len 0\r\n')
#
#
# objTab.Screen.Send('conf t\r\n')
# objTab.Screen.Send('int g0/0\r\n')
# #objTab.Screen.Send('ip address 10.48.16.71 255.255.255.0\r\n')
# objTab.Screen.Send('ip address dhcp\r\n')
# objTab.Screen.Send('no shut\r\n')
# objTab.Screen.Send('end\r\n')
# #objTab.Screen.Send('ip route 0.0.0.0 0.0.0.0 10.48.16.1\r\n')
#
#
#
# Copy_TFTP(Filename='c1900-universalk9-mz.SPA.153-3.M6.bin', TFTP_Server_IP=a)
#
#
#
# Delete_Old_IOS(IOS_To_Keep='c1900-universalk9-mz.SPA.153-3.M6.bin')



#Commands = ['show version', 'dir', 'show run', 'show ip int brief', 'show ip route']

#DCAP(Commands)

#########################################################


def Scrape_Screen():#(a=-1, b=1, Width=100, Height=100):
    objTab = crt.GetScriptTab()
    objTab.Screen.Synchronous = True
    objTab.Screen.IgnoreEscape = True
    #Screen = crt.Screen.Get(-1, 1, Width, Height)
    Screen = crt.Screen.Get(-1, 1, 100, 100)
    return Screen


#########################################################
'''
SCRIPT RUNTIME
'''


#objTab = crt.GetScriptTab()
#objTab.Screen.Synchronous = True
#objTab.Screen.IgnoreEscape = True

#Action_Menu()


if __name__ == '__main__':

    Create_Directory(LOG_DIRECTORY)

    objTab = crt.GetScriptTab()
    objTab.Screen.Synchronous = True
    objTab.Screen.IgnoreEscape = True


    # TFTP server variable; replace with TFTP server IPv4 address

    Login()

    objTab.Screen.Send('term len 0\r\n')

    #Set_Mode(Go_To_Mode='')

    #Current_Mode = Get_Mode() # [Mode, Prompt, Hostname]
    #Hostname = Get_Hostname()
    #Hostname_And_Prompt = Hostname+Current_Mode[1]


    #Get_Inventory(Hostname=Hostname, Debug=False)


    Commands = ['show version', 'dir', 'show inv', 'show run']
    DCAP(Commands)

    '''
    objTab.Screen.Send('dir\r\n')
    Sleep(Seconds=10)
    Alert(Message='Waited for 10 seconds!')
    Dir = Run(Command='dir')
    Alert(Message=Dir)

    Model = Get_Model()
    Alert(Message=Model)
    '''

    #Action_Menu()

    #Delete_Old_IOS(IOS_To_Keep='c1900-universalk9-mz.SPA.153-3.M6.bin', Path='flash:')


    #Ask_IOS()

    #Get_Model(Debug=True)
    #Alert(Message=Get_Model())




