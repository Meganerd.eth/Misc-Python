#!C:\Python27\python.exe
# Filename: Powershell.py
# -*- coding: utf-8 -*-

import os
import re
import time
from datetime import datetime
import datetime as Date
import subprocess

''' POWERSHELL / WINRM / CMDLETS '''

'''
Get-DHCPServerv4scope | Get-DHCPServerv4Lease | Format-Table -Property IPAddress,ClientId

'''

# Get-DHCPServerv4scope | Get-DHCPServerv4Lease
def Powershell(Command='Get-DHCPServerv4scope', Debug=True):
    ''' Powershell subproccess command
    
    + Powershell parameter input
    - Command is string; Input PS CMD and we will run it
    
    + Notes
    - Pipes are supported; Pipes will be parsed automatically
    
    return boolean and shell output as [Successfull, Powershell_Out] '''
    Successful = False

    if type(Command) == tuple:
        Command = list(Command)
    elif type(Command) == str:
        Command = Command.split(' ')
        
    Command.insert(0, 'powershell')
    
    if Debug == True:
        print '\n[*] Powershell> ['+' '.join(Command)+'] :'
        
    Powershell = subprocess.Popen(Command, \
                        stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    Powershell_Out, Powershell_Err = Powershell.communicate() # Stdout results to variable
    #import pdb;pdb.set_trace()

    if len(Powershell_Out) == 0 and len(Powershell_Err) > 0:
        if Debug == True:
            print '\n[!] Command returned error:'
            print Powershell_Err
        return None
    try:
        Powershell_Out = re.findall(Regex_ICMP_Reply, Ping_Out_IP)[0]
    except:
        Powershell_Out = ''
        if Debug == True:
            print '\n\n[!] Powershell> error'
    if len(str(Powershell_Out)) > 1:
        if Debug == True:
            print '[+] Powershell> command success'
        Successful = True
        print Powershell_Out
    return [Successful, Powershell_Out]

def Get_DHCPServerv4Lease_List(ScopeID=0, Debug=False):
    ''' Powershell subproccess return DHCP leases from scopes
    ScopeID 0 will capture all scopes. Any other will capture by VID.
    output formats as list, shows only properties IPAddress, ClientId '''
    Successful = False

    if ScopeID == 0:
        if Debug == True:
            print '\n[*] Powershell> [Get-DHCPServerv4scope | Get-DHCPServerv4Lease] :'
        Powershell = subprocess.Popen(['powershell', 'Get-DHCPServerv4scope', '|', 'Get-DHCPServerv4Lease',
                                       '|', 'Format-List', '-Property', 'IPAddress,', 'ClientId'], \
                            stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    elif not ScopeID == 0:
        if Debug == True:
            print '\n[*] Powershell> [Get-DHCPServerv4Lease -ScopeID %d] :' % ScopeID
        Powershell = subprocess.Popen(['powershell', 'Get-DHCPServerv4Lease',
                                       '|', 'Format-List', '-Property', 'IPAddress,', 'ClientId'], \
                            stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        
    Powershell_Out, Powershell_Err = Powershell.communicate() # Stdout results to variable
    #import pdb;pdb.set_trace()
    try:
        Powershell_Out = re.findall(Regex_PS_Leases_List, Powershell_Out)
    except:
        Powershell_Out = ''
        if Debug == True:
            print '\n\n[!] Powershell> error getting DHCP server leases'
    if len(str(Powershell_Out)) > 0:
        for n in range(0, len(Powershell_Out)):
            Powershell_Out[n] = [Powershell_Out[n][0], Powershell_Out[n][1].upper().replace('-', '')]
        if Debug == True:
            print '[+] Powershell> command success'
            print '[+] Got %d leases' % len(Powershell_Out)
        Successful = True
        #print Powershell_Out[1:10]
    return [Successful, Powershell_Out]

#Get_DHCPServerv4Lease_List(True)
# Get-DHCPServerv4Lease -ScopeID 192.168.80.0 -AllLeases | Format-List -Property IPAddress,ClientId

# Get-DHCPServerv4scope | Get-DHCPServerv4Lease
def Get_DHCPServerv4Lease(Debug=False):
    ''' Powershell subproccess return DHCP leases from all scopes '''
    Successful = False
    if Debug == True:
        print '\n[*] Powershell> [Get-DHCPServerv4scope | Get-DHCPServerv4Lease] :'
    Powershell = subprocess.Popen(['powershell', 'Get-DHCPServerv4scope', '|', 'Get-DHCPServerv4Lease'], \
                        stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    Powershell_Out, Powershell_Err = Powershell.communicate() # Stdout results to variable
    #import pdb;pdb.set_trace()
    try:
        Powershell_Out = re.findall(Regex_ICMP_Reply, Powershell_Out)
    except:
        Powershell_Out = ''
        if Debug == True:
            print '\n\n[!] Powershell> error getting DHCP server leases'
    if len(str(Powershell_Out)) > 1:
        if Debug == True:
            print '[+] Powershell> command success'
        Successful = True
        print Powershell_Out
    return [Successful, Powershell_Out]

#Get_DHCPServerv4Lease(True)

if __name__ == '__main__':
    #import pdb;pdb.set_trace()
    pass
    